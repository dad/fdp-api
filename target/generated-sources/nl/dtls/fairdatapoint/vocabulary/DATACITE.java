package nl.dtls.fairdatapoint.vocabulary;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

public final class DATACITE {
    public static final String NAMESPACE = "";
    public static final String PREFIX = "datacite";

    /** <tt>http://purl.org/spar/datacite/AlternateResourceIdentifier</tt> */
    public static final IRI ALTERNATERESOURCEIDENTIFIER;

    /** <tt>http://purl.org/spar/datacite/PrimaryResourceIdentifier</tt> */
    public static final IRI PRIMARYRESOURCEIDENTIFIER;

    /** <tt>http://www.essepuntato.it/2010/06/literalreification/Literal</tt> */
    public static final IRI LITERAL;

    /** <tt>http://purl.org/spar/datacite/FunderIdentifier</tt> */
    public static final IRI FUNDERIDENTIFIER;

    /** <tt>http://purl.org/spar/datacite/MetadataScheme</tt> */
    public static final IRI METADATASCHEME;

    /** <tt>http://purl.org/spar/datacite/OrganizationIdentifier</tt> */
    public static final IRI ORGANIZATIONIDENTIFIER;

    /** <tt>http://purl.org/spar/datacite/OrganizationIdentifierScheme</tt> */
    public static final IRI ORGANIZATIONIDENTIFIERSCHEME;

    /** <tt>http://purl.org/spar/datacite/PersonalIdentifier</tt> */
    public static final IRI PERSONALIDENTIFIER;

    /** <tt>http://www.w3.org/2004/02/skos/core#Concept</tt> */
    public static final IRI CONCEPT;

    /** <tt>http://purl.org/spar/datacite/AgentIndetifierScheme</tt> */
    public static final IRI AGENTINDETIFIERSCHEME;

    /** <tt>http://purl.org/spar/datacite/FunderIdentifierScheme</tt> */
    public static final IRI FUNDERIDENTIFIERSCHEME;

    /** <tt>http://purl.org/spar/datacite/Identifier</tt> */
    public static final IRI IDENTIFIER;

    /** <tt>http://purl.org/spar/datacite/IdentifierScheme</tt> */
    public static final IRI IDENTIFIERSCHEME;

    /** <tt>http://purl.org/spar/datacite/AgentIdentifier</tt> */
    public static final IRI AGENTIDENTIFIER;

    /** <tt>http://purl.org/spar/datacite/DescriptionType</tt> */
    public static final IRI DESCRIPTIONTYPE;

    /** <tt>http://purl.org/spar/datacite/ResourceIdentifier</tt> */
    public static final IRI RESOURCEIDENTIFIER;

    /** <tt>http://purl.org/spar/datacite/PersonalIdentifierScheme</tt> */
    public static final IRI PERSONALIDENTIFIERSCHEME;

    /** <tt>http://purl.org/spar/datacite/ResourceIdentifierScheme</tt> */
    public static final IRI RESOURCEIDENTIFIERSCHEME;

    /** <tt>http://purl.org/spar/datacite/hasCreatorList</tt> */
    public static final IRI HASCREATORLIST;

    /** <tt>http://purl.org/spar/datacite/hasGeneralResourceType</tt> */
    public static final IRI HASGENERALRESOURCETYPE;

    /** <tt>http://purl.org/spar/datacite/usesMetadataScheme</tt> */
    public static final IRI USESMETADATASCHEME;

    /** <tt>http://purl.org/spar/fabio/hasURL</tt> */
    public static final IRI HASURL;

    /** <tt>http://www.w3.org/2002/07/owl#topObjectProperty</tt> */
    public static final IRI TOPOBJECTPROPERTY;

    /** <tt>http://purl.org/spar/datacite/hasDescription</tt> */
    public static final IRI HASDESCRIPTION;

    /** <tt>http://purl.org/spar/datacite/hasDescriptionType</tt> */
    public static final IRI HASDESCRIPTIONTYPE;

    /** <tt>http://purl.org/dc/terms/type</tt> */
    public static final IRI TYPE;

    /** <tt>http://purl.org/spar/datacite/hasIdentifier</tt> */
    public static final IRI HASIDENTIFIER;

    /** <tt>http://purl.org/spar/datacite/usesIdentifierScheme</tt> */
    public static final IRI USESIDENTIFIERSCHEME;


    static {
        ValueFactory VF = SimpleValueFactory.getInstance();

        ALTERNATERESOURCEIDENTIFIER = VF.createIRI("http://purl.org/spar/datacite/AlternateResourceIdentifier");
        PRIMARYRESOURCEIDENTIFIER = VF.createIRI("http://purl.org/spar/datacite/PrimaryResourceIdentifier");
        LITERAL = VF.createIRI("http://www.essepuntato.it/2010/06/literalreification/Literal");
        FUNDERIDENTIFIER = VF.createIRI("http://purl.org/spar/datacite/FunderIdentifier");
        METADATASCHEME = VF.createIRI("http://purl.org/spar/datacite/MetadataScheme");
        ORGANIZATIONIDENTIFIER = VF.createIRI("http://purl.org/spar/datacite/OrganizationIdentifier");
        ORGANIZATIONIDENTIFIERSCHEME = VF.createIRI("http://purl.org/spar/datacite/OrganizationIdentifierScheme");
        PERSONALIDENTIFIER = VF.createIRI("http://purl.org/spar/datacite/PersonalIdentifier");
        CONCEPT = VF.createIRI("http://www.w3.org/2004/02/skos/core#Concept");
        AGENTINDETIFIERSCHEME = VF.createIRI("http://purl.org/spar/datacite/AgentIndetifierScheme");
        FUNDERIDENTIFIERSCHEME = VF.createIRI("http://purl.org/spar/datacite/FunderIdentifierScheme");
        IDENTIFIER = VF.createIRI("http://purl.org/spar/datacite/Identifier");
        IDENTIFIERSCHEME = VF.createIRI("http://purl.org/spar/datacite/IdentifierScheme");
        AGENTIDENTIFIER = VF.createIRI("http://purl.org/spar/datacite/AgentIdentifier");
        DESCRIPTIONTYPE = VF.createIRI("http://purl.org/spar/datacite/DescriptionType");
        RESOURCEIDENTIFIER = VF.createIRI("http://purl.org/spar/datacite/ResourceIdentifier");
        PERSONALIDENTIFIERSCHEME = VF.createIRI("http://purl.org/spar/datacite/PersonalIdentifierScheme");
        RESOURCEIDENTIFIERSCHEME = VF.createIRI("http://purl.org/spar/datacite/ResourceIdentifierScheme");
        HASCREATORLIST = VF.createIRI("http://purl.org/spar/datacite/hasCreatorList");
        HASGENERALRESOURCETYPE = VF.createIRI("http://purl.org/spar/datacite/hasGeneralResourceType");
        USESMETADATASCHEME = VF.createIRI("http://purl.org/spar/datacite/usesMetadataScheme");
        HASURL = VF.createIRI("http://purl.org/spar/fabio/hasURL");
        TOPOBJECTPROPERTY = VF.createIRI("http://www.w3.org/2002/07/owl#topObjectProperty");
        HASDESCRIPTION = VF.createIRI("http://purl.org/spar/datacite/hasDescription");
        HASDESCRIPTIONTYPE = VF.createIRI("http://purl.org/spar/datacite/hasDescriptionType");
        TYPE = VF.createIRI("http://purl.org/dc/terms/type");
        HASIDENTIFIER = VF.createIRI("http://purl.org/spar/datacite/hasIdentifier");
        USESIDENTIFIERSCHEME = VF.createIRI("http://purl.org/spar/datacite/usesIdentifierScheme");

    }

    /** Utility class; private constructor to prevent instance being created. */
    private DATACITE() {
    }
}