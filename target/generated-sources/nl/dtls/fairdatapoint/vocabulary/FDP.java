package nl.dtls.fairdatapoint.vocabulary;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

public final class FDP {
    public static final String NAMESPACE = "http://rdf.biosemantics.org/ontologies/fdp-o#";
    public static final String PREFIX = "fdp";

    /** <tt>http://rdf.biosemantics.org/ontologies/fdp-o#DataRecord</tt> */
    public static final IRI DATARECORD;

    /** <tt>http://rdf.biosemantics.org/ontologies/fdp-o#MetadataDocument</tt> */
    public static final IRI METADATADOCUMENT;

    /** <tt>http://rdf.biosemantics.org/ontologies/fdp-o#apiVersion</tt> */
    public static final IRI APIVERSION;

    /** <tt>http://rdf.biosemantics.org/ontologies/fdp-o#contains</tt> */
    public static final IRI CONTAINS;

    /** <tt>http://rdf.biosemantics.org/ontologies/fdp-o#dataRecord</tt> */
    public static final IRI HAS_DATARECORD;

    /** <tt>http://rdf.biosemantics.org/ontologies/fdp-o#metadataIdentifier</tt> */
    public static final IRI METADATAIDENTIFIER;

    /** <tt>http://rdf.biosemantics.org/ontologies/fdp-o#rmlInputSource</tt> */
    public static final IRI RMLINPUTSOURCE;

    /** <tt>http://rdf.biosemantics.org/ontologies/fdp-o#rmlMapping</tt> */
    public static final IRI RMLMAPPING;

    /** <tt>http://rdf.biosemantics.org/ontologies/fdp-o#metadataIssued</tt> */
    public static final IRI METADATAISSUED;

    /** <tt>http://rdf.biosemantics.org/ontologies/fdp-o#metadataModified</tt> */
    public static final IRI METADATAMODIFIED;


    static {
        ValueFactory VF = SimpleValueFactory.getInstance();

        DATARECORD = VF.createIRI("http://rdf.biosemantics.org/ontologies/fdp-o#DataRecord");
        METADATADOCUMENT = VF.createIRI("http://rdf.biosemantics.org/ontologies/fdp-o#MetadataDocument");
        APIVERSION = VF.createIRI("http://rdf.biosemantics.org/ontologies/fdp-o#apiVersion");
        CONTAINS = VF.createIRI("http://rdf.biosemantics.org/ontologies/fdp-o#contains");
        HAS_DATARECORD = VF.createIRI("http://rdf.biosemantics.org/ontologies/fdp-o#dataRecord");
        METADATAIDENTIFIER = VF.createIRI("http://rdf.biosemantics.org/ontologies/fdp-o#metadataIdentifier");
        RMLINPUTSOURCE = VF.createIRI("http://rdf.biosemantics.org/ontologies/fdp-o#rmlInputSource");
        RMLMAPPING = VF.createIRI("http://rdf.biosemantics.org/ontologies/fdp-o#rmlMapping");
        METADATAISSUED = VF.createIRI("http://rdf.biosemantics.org/ontologies/fdp-o#metadataIssued");
        METADATAMODIFIED = VF.createIRI("http://rdf.biosemantics.org/ontologies/fdp-o#metadataModified");

    }

    /** Utility class; private constructor to prevent instance being created. */
    private FDP() {
    }
}