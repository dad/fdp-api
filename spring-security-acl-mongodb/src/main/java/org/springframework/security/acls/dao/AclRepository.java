/**
 * The MIT License
 * Copyright © 2017 DTL
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.springframework.security.acls.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.security.acls.domain.MongoAcl;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

/**
 * Spring Data MongoDB aclRepository for {@link MongoAcl} instances.
 *
 * @author Roman Vottner
 * @since 4.3
 */
@Repository
public interface AclRepository extends MongoRepository<MongoAcl, Serializable> {

	/**
	 * Retrieves an access control list by its unique identifier.
	 *
	 * @param id The unique identifier of the access control list to return
	 * @return The ACL instance identified by the given ID
	 */
	Optional<MongoAcl> findById(Serializable id);

	/**
	 * Returns the ACL for a given domain object identifier and its class name.
	 *
	 * @param instanceId The unique identifier of the domain object the ACL should be returned for
	 * @param className  The class name of the domain object referenced by the ACL
	 * @return The access control list for the matching domain object.
	 */
	List<MongoAcl> findByInstanceIdAndClassName(Serializable instanceId, String className);

	/**
	 * Retrieves all child ACLs which specified the given <em>parentId</em> as their parent.
	 *
	 * @param parentId The unique identifier of the parent ACL
	 * @return A list of child ACLs for the given parent ACL ID.
	 */
	List<MongoAcl> findByParentId(Serializable parentId);

	/**
	 * Removes a document from the ACL collection that contains an instanceId field set to the provided value.
	 *
	 * @param instanceId The unique identifier of the domain object to remove an ACL entry for
	 * @return The number of deleted documents
	 */
	Long deleteByInstanceId(Serializable instanceId);
}
