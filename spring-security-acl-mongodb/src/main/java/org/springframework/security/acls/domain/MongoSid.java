/**
 * The MIT License
 * Copyright © 2017 DTL
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.springframework.security.acls.domain;

/**
 * Represents a security identity assignable to certain permissions in an access control list. The identity can either
 * be a user principal or a granted authority. If {@link #isPrincipal} returns true, the security identity represents an
 * authenticated user, otherwise an instance of this class will represent a granted authority.
 *
 * @author Roman Vottner
 * @since 4.3
 */
public class MongoSid {
	/**
	 * The name of the security identity
	 **/
	private String name;
	/**
	 * Defines whether this security identity is a user principal (true) or a granted authority (false)
	 **/
	private boolean isPrincipal;

	/**
	 * Default constructor needed by Spring.
	 */
	public MongoSid() {

	}

	/**
	 * Creates a new security identity which represents a user principal assignable to permissions in an access control
	 * list.
	 *
	 * @param name The name of the user principal this security identity is created for
	 */
	public MongoSid(String name) {
		this.name = name;
		this.isPrincipal = true;
	}

	/**
	 * Creates a new security identity assignable to permissions in an access control list. This constructor differs
	 * from {@link #MongoSid(String)} by allowing to specify the actual type of security identity to create.
	 *
	 * @param name        The name of the user or role this security identity is created for
	 * @param isPrincipal Defines whether this security identity represents a user principal (true) or a granted
	 *                    authority (false)
	 */
	public MongoSid(String name, boolean isPrincipal) {
		this.name = name;
		this.isPrincipal = isPrincipal;
	}

	/**
	 * Returns the name of the security identity. In case {@link #isPrincipal} returns true, this is the user name,
	 * otherwise it will match the role name of the granted authority.
	 *
	 * @return The name of the security identity
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Defines the new name of this security identity instance.
	 *
	 * @param name The name to assign to the security identity.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Defines whether this security identity is a user principal (true) or a granted authority (false).
	 *
	 * @return <em>true</em> in case this instance represents a user principal, <em>false</em> for granted authorities
	 */
	public boolean isPrincipal() {
		return this.isPrincipal;
	}

	/**
	 * Specifies whether this instance is a user principal or a granted authority.
	 *
	 * @param isPrincipal If set to <em>true</em> will mark this security identity instance as a user principal. On
	 *                    providing <em>false</em> this instance will represent a granted authority
	 */
	public void setPrincipal(boolean isPrincipal) {
		this.isPrincipal = isPrincipal;
	}

	@Override
	public String toString() {
		return "MongoSid[name = " + name + ", isPrincipal = " + isPrincipal + "]";
	}
}
